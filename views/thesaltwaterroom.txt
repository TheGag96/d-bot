I opened my eyes last night
And saw you in the low light
Walking down by the bay
On the shore, staring up at the planes
That aren't there anymore
I was feeling the night grow old
And you were looking so cold
So like an introvert
I drew my over shirt
Around my arms and began
To shiver violently before
You happened to look
And see the tunnels all around me
Running into the dark
Underground
All the subways around
Create a great sound
To my motion fatigue
Farewell
With your ear to a seashell
You can hear the waves
In underwater caves
As if you actually were inside
A saltwater room
Time together isn't never quite enough
When you and I are alone
I've never felt so at home
What will it take to make or break this hint of love?
Only time, only time
When we're apart what ever are you thinking of
If this is what I call home why does it feel so alone
So tell me darling do you wish we'd fall in love?
All the time, all the time
Can you believe that the crew has gone
And wouldn't let me sign on?
All my islands have sunk
In the deep, so I can hardly relax
Or even oversleep
I feel as if I were home, some nights
When we count all the ship lights
I guess I'll never know
Why sparrows love the snow
We'll turn off all of the lights
And set this ballroom aglow
Time together isn't never quite enough
When you and I are alone
I've never felt so at home
What will it take to make or break this hint of love?
We need time, only time
When we're apart whatever are you thinking of?
If this is what I call home why does it feel so alone?
So tell me darling, do you wish we'd fall in love?
All the time, all the time
Time together is just never quite enough
When we're apart whatever are you thinking of?
What will it take to make or break this hint of love?
So tell me darling do you wish we'd fall in love?
All the time