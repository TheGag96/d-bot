module main;

import markov;

import std.stdio,
       std.algorithm,
       std.string,
       std.format,
       std.conv,
       std.array,
       std.json,
       std.traits,
       std.process,
       std.file,
       core.time;

import vibe.core.core;
import vibe.http.client;

import dscord.core,
       dscord.util.process,
       dscord.util.emitter;

import core.sys.posix.signal;
import etc.linux.memoryerror;

import dscord.util.string : camelCaseToUnderscores;

Learner learner;

class BasicPlugin : Plugin {
  @Command("owlcity")
  void onOwlCity(CommandEvent event) {
    uint numLines = 10;
    bool error = false;

    if (event.args.length > 0) {
      try {
        numLines = event.args[0].to!uint;
      }
      catch (Exception e) {
        error = true;
      }
    }

    error = error || numLines > 100;

    if (error) {
      event.msg.reply("Please supply a (sensible) number of lines to write (if not provided, it'll be 10).");
      return;
    }

    auto app = appender!string;

    app.put("```\n");
    foreach (i; 0..numLines) {
      app.put(learner.speak());
      app.put("\n");
    }
    app.put("```");

    event.msg.reply(app.data);
  }
}

void init() {
  learner = new Learner;

  dirEntries("./views", SpanMode.breadth)
    .map!(x => x.name)
    .filter!(x => x.endsWith(".txt"))
    .map!readText
    .each!(x => x
      .lineSplitter
      .each!(y => learner.learn(y)));
}

void main(string[] args) {
  static if (is(typeof(registerMemoryErrorHandler)))
      registerMemoryErrorHandler();

  if (args.length <= 1) {
    writefln("Usage: %s <token>", args[0]);
    return;
  }

  init();

  BotConfig config;
  config.cmdRequireMention = false;
  config.token = args[1];
  config.cmdPrefix = "d!";
  Bot bot = new Bot(config, LogLevel.trace);
  bot.loadPlugin(new BasicPlugin);
  bot.run();
  runEventLoop();
  return;
}
