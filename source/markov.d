import std.stdio, std.algorithm, std.array, std.string, std.conv, std.typecons, std.random, std.file, std.regex, std.uni;

class Node {
  string name;
  Edge[] neighbors;

  this(string name) {
    this.name = name;
  }

  Edge findNeighbor(string lookingFor) {
    Edge result = Edge(null, -1);

    foreach (adjacent; this.neighbors) {
      if (adjacent.to.name == lookingFor) {
        result = adjacent;
        break;
      }
    }

    return result;
  }

  Node getRandomNeighbor() {
    return neighbors[dice(this.neighbors.map!(x => x.weight))].to;
  }
}

struct Edge {
  Node to;
  int weight = 1;
}

class Learner {
  private Node start, end;
  private Node[string] nodeList;

  this() {
    start = new Node("");
    end = new Node("<<end>>");
  }

  void learn(R)(R message) {
    Node runner = this.start;

    auto words = message
      .splitter!(Yes.keepSeparators)(ctRegex!`\s+|[?().,";!]`)
      .filter!(x => !x.all!isWhite);

    foreach (word; words) {
      Edge existingEdge = runner.findNeighbor(word);

      if (existingEdge.to is null) {
        Node nodeToConnect;

        if (word in this.nodeList) {
          nodeToConnect = this.nodeList[word];
        }
        else {
          nodeToConnect = new Node(word);
          this.nodeList[word] = nodeToConnect;
        }

        existingEdge = Edge(nodeToConnect);
        runner.neighbors ~= existingEdge;
      }
      else {
        existingEdge.weight++;
      }

      runner = existingEdge.to;
    }

    Edge toEnd = runner.findNeighbor("<<end>>");

    if (toEnd.to is null) {
      runner.neighbors ~= Edge(end);
    }
    else {
      toEnd.weight++;
    }
  }

  string speak() {
    static immutable NO_SPACE_BEFORE = ["?", ",", ".", "!", ";", ")"];
    static immutable NO_SPACE_AFTER  = ["("];

    Node runner = start.getRandomNeighbor;
    auto app = appender!string;

    do {
      app.put(runner.name);
      bool noSpace = NO_SPACE_AFTER.canFind(runner.name);

      runner = runner.getRandomNeighbor;
      noSpace = noSpace || NO_SPACE_BEFORE.canFind(runner.name);

      if (!noSpace) app.put(" ");
    } while (runner.name != "<<end>>");

    return app.data;
  }

  string getEdgeData() {
    static struct DoubleString {
      string from, to;      
    }

    int[DoubleString] edgeMap;
    auto app = appender!string;

    foreach (node; this.nodeList) {
      foreach (neighbor; node.neighbors) {
        edgeMap[DoubleString(node.name, neighbor.to.name)] = neighbor.weight;
      }
    }

    foreach (neighbor; start.neighbors) {
      edgeMap[DoubleString(start.name, neighbor.to.name)] = neighbor.weight;
    }

    app.put("Source;Target;Weight\n");

    foreach (key, value; edgeMap) {
      if (key.from == "") {
        app.put("<<start>>");
      }
      else {
        app.put(key.from);
      }
      app.put(";");
      app.put(key.to);
      app.put(";");
      app.put(value.to!string);
      app.put("\n");
    }

    return app.data;
  }
}

